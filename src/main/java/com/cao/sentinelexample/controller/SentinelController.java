package com.cao.sentinelexample.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * @Author: caozhongying
 * @Date: 2020-4-14 22:39
 * @Version 1.0
 */
@RestController
public class SentinelController {
    @SentinelResource(value="hi")
    @GetMapping("/hi")
    public String hi(@RequestParam(value = "name")String name){
        System.out.println(System.currentTimeMillis()+":"+"hi "+name);
        return "hi "+name;
    }
    @SentinelResource(value="hi2")
    @GetMapping("/hi2")
    public String hi2(@RequestParam(value = "name")String name){
        return "hi2 "+name;
    }

    /**
     * 根据url下载文件，保存到filepath中
     * @return
     */
//    public static void main(String[] args) throws IOException {
//        HttpClient client = HttpClients.createDefault();
//        HttpGet httpGet = new HttpGet("https://2.ddyunbo.com/20200412/oz37tZWF/600kb/hls/GTSfV2Qe.ts");
//
//        //设置请求
//        /**
//         * setConnectTimeout：设置连接超时时间，单位毫秒。
//         *
//         * setConnectionRequestTimeout：设置从connect Manager(连接池)获取Connection 超时时间，单位毫秒。这个属性是新加的属性，因为目前版本是可以共享连接池的。
//         *
//         * setSocketTimeout：请求获取数据的超时时间(即响应时间)，单位毫秒。 如果访问一个接口，多少时间内无法返回数据，就直接放弃此次调用。
//         */
//        RequestConfig requestConfig = RequestConfig.custom()
//                .setConnectTimeout(5000).setConnectionRequestTimeout(1000)
//                .setSocketTimeout(3000).build();
//        httpGet.setConfig(requestConfig);
//        HttpResponse response = client.execute(httpGet);
//        HttpEntity entity = response.getEntity();
//
//        InputStream is = entity.getContent();
//        File file = new File("C:\\xpleaf\\a\\sdsa.mp4");
////        C:\xpleaf\a
//        FileOutputStream fileout = new FileOutputStream(file);
//        /**
//         * 根据实际运行效果 设置缓冲区大小
//         */
//        byte[] buffer = new byte[102400];
//        int ch = 0;
//        while ((ch = is.read(buffer)) != -1) {
//            fileout.write(buffer, 0, ch);
//        }
//        is.close();
//        fileout.flush();
//        fileout.close();
//    }
    public static void main(String[] args) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        //https://2.ddyunbo.com/20190818/RMxEDcRE/800kb/hls/index.m3u8
        HttpGet httpget = new HttpGet("https://2.ddyunbo.com/20190818/RMxEDcRE/800kb/hls/index.m3u8");

        CloseableHttpResponse response = httpClient.execute(httpget);
        HttpEntity httpEntity = response.getEntity();
        String strResult = EntityUtils.toString(httpEntity);
        String[] split = strResult.split("\\n");
        ArrayList<String> urlList = new ArrayList<String>();
        for (String s : split) {
            if (s.contains("20190818")){
                String url = "https://2.ddyunbo.com"+s;
                urlList.add(url);
//                System.out.println(url);
            }
        }

        System.out.println(urlList);
        for (int i=0;i<urlList.size();i++) {
            HttpClient client = HttpClients.createDefault();
            HttpGet httpGet = new HttpGet(urlList.get(i));
            RequestConfig requestConfig = RequestConfig.custom()
                .setConnectTimeout(50000).setConnectionRequestTimeout(10000)
                .setSocketTimeout(30000000).build();
            httpGet.setConfig(requestConfig);
            HttpResponse response1 = client.execute(httpGet);
            HttpEntity entity = response1.getEntity();

            InputStream is = entity.getContent();
            int name = i+1;
            File file = new File("C:\\xpleaf\\a\\"+name+".mp4");
    //        C:\xpleaf\a
            FileOutputStream fileout = new FileOutputStream(file);
            /**
             * 根据实际运行效果 设置缓冲区大小
             */
            byte[] buffer = new byte[102400];
            int ch = 0;
            while ((ch = is.read(buffer)) != -1) {
                fileout.write(buffer, 0, ch);
            }
            is.close();
            fileout.flush();
            fileout.close();
        }
    }
}
